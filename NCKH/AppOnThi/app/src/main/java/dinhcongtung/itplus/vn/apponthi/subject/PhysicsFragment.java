package dinhcongtung.itplus.vn.apponthi.subject;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.exam.Exam;
import dinhcongtung.itplus.vn.apponthi.exam.ExamAdapter;
import dinhcongtung.itplus.vn.apponthi.exam.ExamController;
import dinhcongtung.itplus.vn.apponthi.MainActivity;
import dinhcongtung.itplus.vn.apponthi.R;
import dinhcongtung.itplus.vn.apponthi.slide.ScreenSlideActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhysicsFragment extends Fragment {
    ExamAdapter examAdapter;
    GridView gridView;
    ArrayList<Exam> arrExam = new ArrayList<>();
    ExamController examController;
    public PhysicsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Thi môn lý");
        return inflater.inflate(R.layout.fragment_mon_ly, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        gridView=getActivity().findViewById(R.id.grvBoDe);
        examController = new ExamController(getContext());
        arrExam = examController.getExam("ly");
        examAdapter = new ExamAdapter(getActivity(),arrExam);
        gridView.setAdapter(examAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent it = new Intent(getActivity(), ScreenSlideActivity.class);
                it.putExtra("num_exam", i+1);
                it.putExtra("subject", "ly");
                startActivity(it);
            }
        });
    }
}
