package dinhcongtung.itplus.vn.apponthi.subject;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.exam.Exam;
import dinhcongtung.itplus.vn.apponthi.exam.ExamAdapter;
import dinhcongtung.itplus.vn.apponthi.exam.ExamController;
import dinhcongtung.itplus.vn.apponthi.MainActivity;
import dinhcongtung.itplus.vn.apponthi.R;
import dinhcongtung.itplus.vn.apponthi.slide.ScreenSlideActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChemistryFragment extends Fragment {
    ExamAdapter examAdapter;
    GridView gridView;
    ArrayList<Exam> arrExam = new ArrayList<>();
    View contentView;
    ExamController examController;
    public ChemistryFragment() {
        // Required empty public constructor
    }

    // chi lam viec voi tang view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (contentView==null){
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("Thi môn hóa");
            contentView=inflater.inflate(R.layout.fragment_mon_hoa, container, false);
        }
        // Inflate the layout for this fragment
        return contentView;
    }
    // dung de viet lenh

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        gridView=getActivity().findViewById(R.id.grvBoDe);
       examController = new ExamController(getContext());
        arrExam = examController.getExam("hoa");
//        arrExam.add(new Exam("Đề số 1"));
//        arrExam.add(new Exam("Đề số 2"));
//        arrExam.add(new Exam("Đề số 3"));
        examAdapter = new ExamAdapter(getActivity(),arrExam);
        gridView.setAdapter(examAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent it = new Intent(getActivity(), ScreenSlideActivity.class);
                // truyền bộ đề đã chọn
                it.putExtra("num_exam", i+1);
                it.putExtra("subject", "hoa");
                startActivity(it);
            }
        });
    }
}
