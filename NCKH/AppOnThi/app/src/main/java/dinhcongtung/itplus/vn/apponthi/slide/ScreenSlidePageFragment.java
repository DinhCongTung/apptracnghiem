package dinhcongtung.itplus.vn.apponthi.slide;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.R;
import dinhcongtung.itplus.vn.apponthi.question.Question;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScreenSlidePageFragment extends Fragment {

    public static final String  ARG_PAGE = "page";
    public static final String  ARG_CHECKFINISH ="checkFinish";
    private int mPageNumber; // vi tri trang hien tai
    private int checkFinish ; // vi tri cau dung
    ArrayList<Question> arrayList;


    TextView txtSoCau;
    TextView txtQuestion;
    RadioGroup radioGroup;
    RadioButton rdoA, rdoB, rdoC, rdoD;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);
        txtSoCau = (TextView)rootView.findViewById(R.id.txtSoCau);
        txtQuestion = (TextView)rootView.findViewById(R.id.txtCauHoi);
        rdoA = (RadioButton) rootView.findViewById(R.id.txtDapAnA);
        rdoB = (RadioButton)rootView.findViewById(R.id.txtDapAnB);
        rdoC = (RadioButton)rootView.findViewById(R.id.txtDapAnC);
        rdoD = (RadioButton)rootView.findViewById(R.id.txtDapAnD);
        radioGroup =(RadioGroup)rootView.findViewById(R.id.rdoGroup);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        ScreenSlideActivity slideActivity = (ScreenSlideActivity) getActivity();
        arrayList=slideActivity.getData();
        mPageNumber=getArguments().getInt(ARG_PAGE);
        checkFinish = getArguments().getInt(ARG_CHECKFINISH);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtSoCau.setText("Câu hỏi số  "+(mPageNumber+1));
        txtQuestion.setText((Html.fromHtml(arrayList.get(mPageNumber).getQuestion())));
        rdoA.setText(Html.fromHtml(arrayList.get(mPageNumber).getAns_a()));
        rdoB.setText(Html.fromHtml(arrayList.get(mPageNumber).getAns_b()));
        rdoC.setText(Html.fromHtml(arrayList.get(mPageNumber).getAns_c()));
        rdoD.setText(Html.fromHtml(arrayList.get(mPageNumber).getAns_d()));

        if(checkFinish==1){
            rdoA.setClickable(false);
            rdoB.setClickable(false);
            rdoC.setClickable(false);
            rdoD.setClickable(false);
            trueAnswer(getItem(mPageNumber).getResult());

        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
           //     Toast.makeText(getActivity(), "Đáp án: " + i , Toast.LENGTH_LONG).show();
                arrayList.get(mPageNumber).choiceID=i;
                arrayList.get(mPageNumber).setAnswer(getChoiceID(i));
            }
        });
    }

    public Question getItem(int pos){
        return arrayList.get(pos);
    }


    // lấy giá trị của radiobutton chuyener thành đáp án ABCD.
    private String getChoiceID(int ID){
        if(ID==R.id.txtDapAnA){
            return "A";
        }else if(ID==R.id.txtDapAnB){
            return "B";
        }else if(ID==R.id.txtDapAnC){
            return "C";
        }else if(ID==R.id.txtDapAnD){
            return "D";
        }

        return "";
    }

    public static ScreenSlidePageFragment create(int pageNumber, int trueAnswer){
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        args.putInt(ARG_CHECKFINISH, trueAnswer);
        fragment.setArguments(args);
        return fragment;
    }

    // kiem tra cau dug tuong ung voi radio nao
    private void trueAnswer(String ans){
        if(ans.equals("A")==true){
            rdoA.setBackgroundColor(Color.RED);
        }else if(ans.equals("B")==true){
            rdoB.setBackgroundColor(Color.RED);
        }else if(ans.equals("C")==true){
            rdoC.setBackgroundColor(Color.RED);
        }else if(ans.equals("D")==true){
            rdoD.setBackgroundColor(Color.RED);
        }else;
    }
}
