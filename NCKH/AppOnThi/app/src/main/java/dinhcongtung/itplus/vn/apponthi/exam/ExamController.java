package dinhcongtung.itplus.vn.apponthi.exam;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.question.DBHelper;

public class ExamController {
    private DBHelper dbHelper;
    public ExamController(Context context){
        dbHelper = new DBHelper(context);
        try {
            dbHelper.openDataBase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Exam> getExam(String sub){
        ArrayList<Exam> data = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select distinct num_exam from n where subject = '"+sub+"'", null);
        cursor.moveToFirst();
        do{
            Exam item;
            item = new Exam("Đề số "+cursor.getString(0));
            data.add(item);
        }while (cursor.moveToNext());
        return data;
    }

}
