package dinhcongtung.itplus.vn.apponthi.checkanswer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.R;
import dinhcongtung.itplus.vn.apponthi.question.Question;

public class CheckAnswerAdapter  extends BaseAdapter {
    ArrayList arrayListAnswer;
    LayoutInflater inflater;

    public CheckAnswerAdapter(ArrayList arrayListAnswer, Context context) {
        this.arrayListAnswer = arrayListAnswer;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return arrayListAnswer.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayListAnswer.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Question question = (Question) getItem(i);
        ViewHolder holder;
        if(view == null){
            holder = new ViewHolder();
            view=inflater.inflate(R.layout.item_gridview_answer,null);
            holder.tvNumAns=view.findViewById(R.id.tvNumAns);
            holder.tvAns=view.findViewById(R.id.tvAns);
            view.setTag(holder);
        }else {
            holder= (ViewHolder) view.getTag();
        }
        int pos = i+1;
        holder.tvNumAns.setText("Câu " + pos+": ");
        holder.tvAns.setText(question.getAnswer());
        return view;
    }

    private static class ViewHolder{
        TextView tvNumAns, tvAns;


    }
}
