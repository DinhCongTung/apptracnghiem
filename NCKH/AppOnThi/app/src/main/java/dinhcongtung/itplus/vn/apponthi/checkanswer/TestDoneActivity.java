package dinhcongtung.itplus.vn.apponthi.checkanswer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.R;
import dinhcongtung.itplus.vn.apponthi.question.Question;

public class TestDoneActivity extends AppCompatActivity {
    ArrayList<Question> arrQues = new ArrayList<>();
    int numTrue=0;
    int numFalse=0;
    int numNull=0;
    TextView tvTrue, tvFalse, tvNull, tvTotal;
    Button btnSave, btnExit, btnReset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_done);


        Intent it = getIntent();
        arrQues = (ArrayList<Question>) it.getExtras().getSerializable("arrQues");

        tvTrue = findViewById(R.id.tvNumTrue);
        tvFalse = findViewById(R.id.tvNumFalse);
        tvNull = findViewById(R.id.tvNumNull);
        tvTotal = findViewById(R.id.tvTotalPoint);
        btnExit = findViewById(R.id.btnExit);
        btnReset = findViewById(R.id.btnReset);
        btnSave = findViewById(R.id.btnSaveScore);

        checkResult();

        tvNull.setText(numNull+"");
        tvFalse.setText(numFalse+"");
        tvTrue.setText(numTrue+"");
        tvTotal.setText(numTrue*10+"");

    }

    private void checkResult(){
        for(int i=0;i<arrQues.size();i++){
            if(arrQues.get(i).getAnswer().equals("")==true){
                numNull++;
            }else if(arrQues.get(i).getResult().equals(arrQues.get(i).getAnswer())==true){
                numTrue++;
            }else numFalse++;
        }
    }
}
