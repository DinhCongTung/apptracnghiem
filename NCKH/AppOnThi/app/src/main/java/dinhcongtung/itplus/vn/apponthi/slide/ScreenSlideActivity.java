package dinhcongtung.itplus.vn.apponthi.slide;

import android.app.Dialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import dinhcongtung.itplus.vn.apponthi.R;
import dinhcongtung.itplus.vn.apponthi.question.Question;
import dinhcongtung.itplus.vn.apponthi.question.QuestionController;
import dinhcongtung.itplus.vn.apponthi.checkanswer.CheckAnswerAdapter;
import dinhcongtung.itplus.vn.apponthi.checkanswer.TestDoneActivity;

public class ScreenSlideActivity extends FragmentActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 10;
    private TextView tvTimer;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    List<Fragment> fragments;

    // lam viec voi csdl
    QuestionController cauhoiController;
    ArrayList<Question> arrayList;

    CounterClass timer;
    String subject;
    int num_exam;
    int totalTimer;
    // bắt sự kiện kiểm tra
    TextView tvCheck;
    // swj kien xem diem
    TextView tvScore;
    public int checkFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        tvTimer = findViewById(R.id.tvTimer);
        tvScore = findViewById(R.id.tvScore);
        fragments=new ArrayList<>();
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),fragments);
        mPager.setAdapter(mPagerAdapter);
        //lenh goi
        mPager.setPageTransformer(true, new DepthPageTransformer());


        // xử lý chọn bộ đề
        Intent it = getIntent();
        subject = it.getStringExtra("subject");
        num_exam = it.getIntExtra("num_exam",0);


        totalTimer=1;
        timer = new CounterClass(totalTimer*60*1000, 1000);
        // csdl
        cauhoiController = new QuestionController(this);
        arrayList = new ArrayList<>();
        arrayList = cauhoiController.getQuestions(num_exam, subject);

        tvCheck = findViewById(R.id.tvKiemTra);

        tvCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer();
            }
        });
        tvTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        tvScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(ScreenSlideActivity.this, TestDoneActivity.class);
                it.putExtra("arrQues", arrayList);
                startActivity(it);
            }
        });
        timer.start();
    }

    // lay cau hoi dua vao frament


    public ArrayList<Question> getData() {
        return arrayList;
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        List<Fragment> fragments;
        public ScreenSlidePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments=fragments;
        }

        // truyeenf vao vi tri cua trang
        @Override
        public Fragment getItem(int position) {
            // checkfinish bang 0 la chua nop bai
            return ScreenSlidePageFragment.create(position, checkFinish);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    public void checkAnswer(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.check_answer_dialog);

        dialog.setTitle("Danh sách câu trả lời");
        CheckAnswerAdapter answerAdapter = new CheckAnswerAdapter(arrayList, this);
        GridView gridView = dialog.findViewById(R.id.grvAnswer);
        gridView.setAdapter(answerAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPager.setCurrentItem(i);
                dialog.dismiss();
            }
        });

        Button btnCancle, btnSubmit;
        btnCancle = dialog.findViewById(R.id.btnCancle);
        btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                timer.cancel();
                viewScore();
                dialog.dismiss();

            }
        });
        dialog.show();
    }
    private void viewScore(){
        if(mPager.getCurrentItem()>=5) mPager.setCurrentItem(mPager.getCurrentItem()+5);
        else mPager.setCurrentItem(mPager.getCurrentItem()-4);
        // neu bang 1 thi la da an nop bai
        checkFinish=1;
        tvScore.setVisibility(View.VISIBLE);
        tvCheck.setVisibility(View.GONE);

    }


    public class CounterClass extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            String countTime = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
            tvTimer.setText(countTime); //SetText cho textview hiện thị thời gian.
        }

        @Override
        public void onFinish() {
            tvTimer.setText("00:00");  //SetText cho textview hiện thị thời gian.
            Intent it = new Intent(ScreenSlideActivity.this, TestDoneActivity.class);
            it.putExtra("arrQues", arrayList);
            startActivity(it);

        }
    }
}