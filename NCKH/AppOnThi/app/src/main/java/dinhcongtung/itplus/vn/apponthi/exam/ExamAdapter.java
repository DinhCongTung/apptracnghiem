package dinhcongtung.itplus.vn.apponthi.exam;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dinhcongtung.itplus.vn.apponthi.R;

public class ExamAdapter extends ArrayAdapter<Exam> {
    public ExamAdapter(@NonNull Context context, ArrayList<Exam> exams) {
        super(context,0, exams);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_gridview, parent, false);
        }
        TextView tvName = convertView.findViewById(R.id.tvNumGrid);
        ImageView imgView = convertView.findViewById(R.id.iconGrid);
        Exam exam = getItem(position);
        if(exam != null){
            tvName.setText(""+exam.getName());
            imgView.setImageResource(R.drawable.iconbode);
        }
        return convertView;
    }
}
